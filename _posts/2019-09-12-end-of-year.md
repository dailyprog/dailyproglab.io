---
layout: post
title:  "Year 2019 recap"
date:   2019-12-17 18:00:00 -0500
categories: announcements
---

It's been an interesting year at Dailyprog ...

At the end of summer, Mabynogy decided to part ways in order to dedicate his free time toward helping women in need. Concretely, this meant not renewing our lease on our shared servers : ``dailyprog`` and ``hackerchan``. In a few IRC chat sessions, some of us decided to get on without servers for the time being. They were not really used as much as they could have anyways ... Instead, keeping the website up and running became our top priority. Kudos to [D3d1rty](https://dailyprog.org/members/d3d1rty.html) who really saved the day by building a replacement website and putting it online within two weeks' notice. 

Dailyprog's website is now a blazing fast static website built with Jekyll as static site generator.

We wish an awesome year 2020 to all of our members : may your side project be a dailyprog's project as well !

- [socraticdev](https://dailyprog.org/members/socraticdev.html)
  
kenster proved it is possible to build a fully functional game in [Nim](https://nim-lang.org/) in 72 hours in the [Ludum Dare challenge](https://ldjam.com). Ludum dare is a competition for building a game in a short amount of time. Together with a couple of other dudes, kenster and a couple of mates managed to get a pretty neat game done relatively quickly. Check it out here: [Succulentia](https://ldjam.com/events/ludum-dare/45/$168872)
  
The next Ludum Dare is Friday April 17th to Monday April 20th, 2020. I wonder if any dailyproggers will jump onto this challenge next time?!  

- [kenster](https://dailyprog.org/members/kenster.html)
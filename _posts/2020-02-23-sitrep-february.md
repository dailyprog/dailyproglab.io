---
layout: post
title:  "sitrep for February 2020"
date:   2020-02-23 12:00:00 -0500
categories: announcements
---

## Here are some news about dailyprog's regulars :

- [kenster](https://dailyprog.org/members/kenster.html) is still following the script !
This newly minted CEO did build and start showing a demo of his product to interested parties.

- [Googlebot](Googlebot) started integrating TSWF to a very cool looking Winamp web client. Ask him about it !

- [Tekdude](Tekdude) is satisfied with the latest version of his music app and will be demoing it. 
  You can have a look at this cool app right now : [https://fleetingpoison.com/](https://fleetingpoison.com/)
  
- [socraticdev](https://dailyprog.org/members/socraticdev.html) is pushing the envelope by 
giving a lightning talk at a local meetup. He is talking about how different it is to code
for a static site. Especially how to figure out where to add that line of code to generate a 
proper url slug despite the presence of diacritics characters in the original string.
    
    - socraticdev also added a cat emoji to the chat section of dailyprog's website.
      In french the word 'chat' means cat.

---
layout: default
name: Aaron Swartz
image-url: "/assets/swartz.jpg"
wiki-url: https://en.wikipedia.org/wiki/Aaron_Swartz
blurb: Aaron Swartz was an American computer programmer, entrepreneur, writer, political organizer, and Internet hacktivist.
---

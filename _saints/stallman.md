---
layout: default
name: Richard Stallman
image-url: "/assets/rms.png"
wiki-url: https://en.wikipedia.org/wiki/Richard_Stallman
blurb: Richard Stallman launched the GNU Project, founded the Free Software Foundation, developed the GNU Compiler Collection and GNU Emacs, and wrote the GNU General Public License.
---

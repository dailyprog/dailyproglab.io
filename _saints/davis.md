---
layout: default
name: Terry A. Davis
image-url: "/assets/terry.png"
wiki-url: https://en.wikipedia.org/wiki/Terry_A._Davis
blurb: Terry Davis was an American programmer who created and designed the operating system TempleOS.
---

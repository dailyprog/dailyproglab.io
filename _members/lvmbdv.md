---
layout: member
title: LVMBDV
handle: lvmbdv
nationality: Turkish 🇹🇷
specializations: C/C++, Rust, Clojure, JavaScript
---

Full stack software engineer, currently (2019 Q4) interested in systems
programming and programming language design.

- Blog: [ata.gitlab.io](https://ata.gitlab.io)
- Gitlab: [@Ata](https://gitlab.com/Ata)
- Github: [@LVMBDV](https://github.com/LVMBDV)
- Twitter: [@LVMBDV](https://twitter.com/LVMBDV)
- Email: [ata.kuyumcu@yandex.ru](mailto:ata.kuyumcu@yandex.ru)
